﻿using System;
using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class Department
    {
        public int DepartmentId { get; set; }

        public string Name { get; set; }

        public decimal Budget { get; set; }

        public DateTime StartDate { get; set; }

        public int? InstructorId { get; set; }

        public virtual Instructor Instructor { get; set; }
        public virtual ICollection<Course> Courses { get; set; }

        public Department()
        {
            Courses = new List<Course>();
        }
    }
}