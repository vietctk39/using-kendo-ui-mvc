﻿using System;
using System.Collections.Generic;
namespace DataAccess.Entities
{
    public class Instructor
    {
        public int Id { get; set; }

        public string LastName { get; set; }

        public string FirstMidName { get; set; }

        public DateTime HireDate { get; set; }

        public virtual OfficeAssignment OfficeAssignment { get; set; }

        public virtual IList<Course> Courses { get; set; }
        public virtual IList<Department> Departments { get; set; }

        public Instructor()
        {
            Courses = new List<Course>();
            Departments = new List<Department>();
        }
    }
}