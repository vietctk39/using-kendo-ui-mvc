﻿using StructureMap.Configuration.DSL;

namespace Website.Business
{
    public class ControllerRegistry: Registry
    {
        public ControllerRegistry()
        {
            Scan(s =>
            {
                s.TheCallingAssembly();
                s.With(new ControllerConvention());
            });
        }
    }
}