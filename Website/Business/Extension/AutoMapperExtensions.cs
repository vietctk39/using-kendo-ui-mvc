﻿using AutoMapper;

namespace Website.Business.Extension
{
    public static class AutoMapperExtensions
    {
        public static TDest MapTo<TSource, TDest>(this TSource source)
        {
            return Mapper.Map<TSource, TDest>(source);
        }

        public static TDest MapTo<TSource, TDest>(this TSource source, TDest dest)
        {
            return Mapper.Map(source, dest);
        }
    }
}