﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StructureMap.Configuration.DSL;

namespace Website.Business.ModelMetadata
{
    public class ModelMetadataRegistry : Registry
    {
        public ModelMetadataRegistry()
        {
            For<ModelMetadataProvider>().Use<ExtensibleModelMetadataProvider>();
            Scan(s =>
            {
                s.TheCallingAssembly();
                s.AddAllTypesOf<IModelMetadataFilter>();
            });
        }
    }
}