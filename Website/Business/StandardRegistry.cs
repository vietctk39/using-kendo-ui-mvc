﻿using StructureMap.Configuration.DSL;

namespace Website.Business
{
    public class StandardRegistry : Registry
    {
        public StandardRegistry()
        {
            Scan(s =>
            {
                s.TheCallingAssembly();
                s.WithDefaultConventions();
            });
        }
    }
}