namespace Website.Business.Task
{
	public interface IRunAfterEachRequest
	{
		void Execute();
	}
}