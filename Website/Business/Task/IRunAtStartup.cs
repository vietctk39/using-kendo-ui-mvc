namespace Website.Business.Task
{
	public interface IRunAtStartup
	{
		void Execute();
	}
}