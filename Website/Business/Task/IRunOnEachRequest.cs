namespace Website.Business.Task
{
	public interface IRunOnEachRequest
	{
		void Execute();
	}
}