namespace Website.Business.Task
{
	public interface IRunOnError
	{
		void Execute();
	}
}