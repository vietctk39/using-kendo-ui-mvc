﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using DataAccess.Contexts;
using DataAccess.Entities;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Website.Business;
using Website.Business.Extension;
using Website.Models;
using Website.Services;

namespace Website.Controllers
{

    public class HomeController : WebsiteController
    {
        private readonly IInstructorsService _instructorsService;

        public HomeController(IInstructorsService instructorsService)
        {
            _instructorsService = instructorsService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Instructor_Read([DataSourceRequest]DataSourceRequest request)
        {
            var data = _instructorsService.GetAllInstructorModel();
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DetailTemplate_HierarchyBinding_Courses([DataSourceRequest] DataSourceRequest request, int id)
        {
            var data = _instructorsService.GetCourseModelById(id);
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateInstructorModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Loi!!");
                return View(model);
            }
            try
            {
                _instructorsService.CreateInstructor(model);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.GetBaseException().Message);
                return View(model);
            }
        }

        public ActionResult Edit(int? id)
        {
            var model = _instructorsService.GetUpdateInstructorModel(id);
            if (model == null)
            {
                ModelState.AddModelError("", "Loi du lieu!!");
                return View("Index");
            }
            ViewBag.Courses = _instructorsService.PopulateAssignedCourseData(model.SelectedCourses.ToList());
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UpdateInstructorModel model)
        {
            ViewBag.Courses = _instructorsService.PopulateAssignedCourseData(model.SelectedCourses);
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Loi du lieu!!");
                return View(model);
            }
            try
            {
                _instructorsService.UpdateInstructor(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.GetBaseException().Message);
                return View(model);
            }
        }

        public ActionResult Delete(int? id)
        {
            var model = _instructorsService.GetDeleteInstructorModel(id);
            if (model == null)
            {
                ModelState.AddModelError("", "Loi du lieu!!");
                return View("Index");
            }
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                _instructorsService.DeleteInstructor(id);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Loi du lieu!!");
            }
            return View();
        }

        public ActionResult Details(int? id)
        {
            var item = _instructorsService.GetDetailInstructor(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }
    }
}