﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using DataAccess.Entities;
using Website.Business.Mapping;

namespace Website.Models
{
    public class AssignedCourseData : IMapFrom<Course>
    {
        public int CourseId { get; set; }
        public string Title { get; set; }
        public bool Assigned { get; set; }
    }
}