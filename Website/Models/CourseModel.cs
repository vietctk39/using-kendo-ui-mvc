﻿using DataAccess.Entities;
using Website.Business.Mapping;

namespace Website.Models
{
    public class CourseModel : IMapFrom<Course>
    {
        public int CourseId { get; set; }

        public string Title { get; set; }
    }
}