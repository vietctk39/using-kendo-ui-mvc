﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DataAccess.Contexts;
using DataAccess.Entities;
using Website.Business.Mapping;

namespace Website.Models
{
    public class CreateInstructorModel : IHaveCustomMappings
    {
        [Required, StringLength(50, MinimumLength = 1)]
        public string LastName { get; set; }

        [Required, StringLength(50, MinimumLength = 1)]
        public string FirstMidName { get; set; }

        [Required, DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime HireDate { get; set; }
        [UIHint("ListOfficeAssignments")]
        public string OfficeAssignment { get; set; }

        [UIHint("ListCourse")]
        public IList<int> SelectedCourses { get; set; }

        public CreateInstructorModel()
        {
            SelectedCourses = new List<int>();
        }

        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<CreateInstructorModel, Instructor>()
                .ForMember(x => x.OfficeAssignment, o => o.MapFrom(x => new OfficeAssignment
                    {
                        Location = x.OfficeAssignment
                    }
                ))
                .AfterMap((model, entity) =>
                {
                    if (model.SelectedCourses.Any())
                    {
                        var courses = DependencyResolver.Current.GetService<SchoolContext>().Courses
                            .Where(x => model.SelectedCourses.Contains(x.CourseId))
                            .ToList();

                        foreach (var courseToAdd in courses)
                        {
                            entity.Courses.Add(courseToAdd);
                        }
                    }
                })
                ;
        }
    }
}