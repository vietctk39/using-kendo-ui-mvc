﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using DataAccess.Entities;
using Website.Business.Mapping;

namespace Website.Models
{
   
    public class DeleteInstructorModel : IHaveCustomMappings
    {
        [ReadOnly(true)]
        public int Id { get; set; }
        [ReadOnly(true)]
        public string LastName { get; set; }
        [ReadOnly(true)]
        public string FirstMidName { get; set; }
        [ReadOnly(true)]
        public DateTime HireDate { get; set; }
        [ReadOnly(true)]
        public string OfficeAssignment { get; set; }
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Instructor, DeleteInstructorModel>()
                .ForMember(x => x.OfficeAssignment, o => o.MapFrom(x => x.OfficeAssignment.Location));
        }
    }
}