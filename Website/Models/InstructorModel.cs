﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using DataAccess.Entities;
using Website.Business.Mapping;

namespace Website.Models
{
    public class InstructorModel : IHaveCustomMappings
    {
        public int Id { get; set; }

        public string LastName { get; set; }

        public string FirstMidName { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime HireDate { get; set; }
        public string OfficeAssignmentLocation { get; set; }


        public IList<CourseModel> Courses { get; set; } = new List<CourseModel>();
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Instructor, InstructorModel>();
        }
    }
}
