﻿namespace Website.Models
{
    public class InstructorQueryParameters
    {
        public int? Id { get; set; }
        public int? CourseId { get; set; }
    }
}