﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using DataAccess.Contexts;
using DataAccess.Entities;
using Website.Business.Mapping;

namespace Website.Models
{
    public class UpdateInstructorModel : IHaveCustomMappings
    {
        [HiddenInput]
        public int Id { get; set; }
        [Required, StringLength(50, MinimumLength = 1)]
        public string LastName { get; set; }

        [Required, StringLength(50, MinimumLength = 1)]
        public string FirstMidName { get; set; }

        [Required,DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),DataType(DataType.Date) ]
        public DateTime HireDate { get; set; }

        public string OfficeAssignment { get; set; }
        [DataType("ListCourse")]
        public List<int> SelectedCourses { get; set; }

        public UpdateInstructorModel()
        {
            SelectedCourses = new List<int>();
        }

        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Instructor, UpdateInstructorModel>()
                .ForMember(x => x.OfficeAssignment, o => o.MapFrom(x => x.OfficeAssignment.Location))
                .ForMember(x => x.SelectedCourses, o => o.MapFrom(x => x.Courses.Select(y => y.CourseId).ToList()));

            configuration.CreateMap<UpdateInstructorModel, Instructor>()
                .ForMember(x => x.OfficeAssignment, o => o.MapFrom(x => new OfficeAssignment
                {
                    Location = string.IsNullOrWhiteSpace(x.OfficeAssignment) ? null : x.OfficeAssignment
                }))
                .AfterMap((model, entity) =>
                {
                    var courses = DependencyResolver.Current.GetService<SchoolContext>().Courses.ToList();

                    if (model.SelectedCourses == null)
                    {
                        entity.Courses = new List<Course>();
                        return;
                    }
                    //List mới
                    var selectedCoursesHS = new HashSet<int>(model.SelectedCourses);
                    //List cũ
                    var instructorCourses = new HashSet<int>(entity.Courses.Select(c => c.CourseId));

                    foreach (var course in courses)
                    {
                        if (selectedCoursesHS.Contains(course.CourseId))
                        {
                            if (!instructorCourses.Contains(course.CourseId))
                            {
                                entity.Courses.Add(course);
                            }
                        }
                        else
                        {
                            if (instructorCourses.Contains(course.CourseId))
                            {
                                entity.Courses.Remove(course);
                            }
                        }
                    }
                });
        }
    }
}