﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper.QueryableExtensions;
using DataAccess.Contexts;
using DataAccess.Entities;
using Website.Business.Extension;
using Website.Models;
using Website.ViewModel;

namespace Website.Services
{
    public interface IInstructorsService
    {
        List<AssignedCourseData> PopulateAssignedCourseData(IEnumerable<int> courseIds);
        UpdateInstructorModel GetUpdateInstructorModel(int? id);
        DeleteInstructorModel GetDeleteInstructorModel(int? id);
        void CreateInstructor(CreateInstructorModel model);
        void UpdateInstructor(UpdateInstructorModel model);
        void DeleteInstructor(int id);
        InstructorIndexData GetInstructors(InstructorQueryParameters parameters);
        DeleteInstructorModel GetDetailInstructor(int? id);
        IQueryable<InstructorModel> GetAllInstructorModel();
        IQueryable<CourseModel> GetCourseModelById(int id);
    }

    public class InstructorsService : IInstructorsService
    {
        private readonly SchoolContext _context;

        public InstructorsService(SchoolContext context)
        {
            _context = context;
        }

        public List<AssignedCourseData> PopulateAssignedCourseData(IEnumerable<int> courseIds)
        {
            var lst = _context.Courses.Project().To<AssignedCourseData>().ToList();

            if (courseIds != null)
            {
                var instructorCourses = new HashSet<int>(courseIds);

                foreach (var item in lst)
                {
                    item.Assigned = instructorCourses.Contains(item.CourseId);
                }
            }

            return lst;
        }

        public UpdateInstructorModel GetUpdateInstructorModel(int? id)
        {
            var model = _context.Instructors
                .Include(i => i.OfficeAssignment)
                .Include(i => i.Courses)
                .Project().To<UpdateInstructorModel>()
                .FirstOrDefault(i => i.Id == id);
            return model;
        }

        public DeleteInstructorModel GetDeleteInstructorModel(int? id)
        {
            var model = new DeleteInstructorModel();
                
            if (id != null)
            {
               model = _context.Instructors.Where(x => x.Id == id).Project().To<DeleteInstructorModel>().FirstOrDefault();
            }

            return model;
        }
        public void CreateInstructor(CreateInstructorModel model)
        {
            var instructor = model.MapTo<CreateInstructorModel, Instructor>();
            _context.Instructors.Add(instructor);
            _context.SaveChanges();
        }
        public void UpdateInstructor(UpdateInstructorModel model)
        {
            var instructorToUpdate = _context.Instructors
                .Include(i => i.OfficeAssignment)
                .Include(i => i.Courses)
                .SingleOrDefault(i => i.Id == model.Id);
            model.MapTo(instructorToUpdate);
            _context.SaveChanges();
        }

        public void DeleteInstructor(int id)
        {
            Instructor instructor = _context.Instructors
                .Include(i => i.OfficeAssignment)
                .Include(i => i.Courses)
                .SingleOrDefault(i => i.Id == id);

            var department = _context.Departments
                .Where(d => d.InstructorId == id).ToList();
            _context.Departments.RemoveRange(department);

            if (instructor != null)
            {
                _context.Instructors.Remove(instructor);
            }
            _context.SaveChanges();
        }

        public InstructorIndexData GetInstructors(InstructorQueryParameters parameters)
        {
            var viewModel = new InstructorIndexData();
            viewModel.Instructors = _context.Instructors
                .Include(i => i.OfficeAssignment)
                .Include(i => i.Courses.Select(c => c.Department))
                .OrderByDescending(i => i.Id).ToList();

            if (parameters.Id.HasValue)
            {
                viewModel.Courses = viewModel.Instructors.Single(i => i.Id == parameters.Id).Courses;
            }

            if (parameters.CourseId.HasValue)
            {
                //ViewBag.CourseID = courseID.Value;
                //viewModel.Enrollments = viewModel.SelectedCourses.Single(x => x.CourseID == courseID).Enrollments;

                var selectedCourse = viewModel.Courses.Single(x => x.CourseId == parameters.CourseId);
                _context.Entry(selectedCourse).Collection(x => x.Enrollments).Load();
                foreach (var enrollment in selectedCourse.Enrollments)
                {
                    _context.Entry(enrollment).Reference(x => x.Student).Load();
                }

                viewModel.Enrollments = selectedCourse.Enrollments;
            }

            viewModel.Parameters = parameters;
            return viewModel;
        }
        public DeleteInstructorModel GetDetailInstructor(int? id)
        {
            var item = new DeleteInstructorModel();
            if (id != null)
            {
                item = _context.Instructors.Where(x => x.Id == id).Project().To<DeleteInstructorModel>().FirstOrDefault();
            }
            return item;
        }


        public IQueryable<InstructorModel> GetAllInstructorModel()
        {
            var data = _context.Instructors.Project().To<InstructorModel>();
            return data;
        }

        public IQueryable<CourseModel> GetCourseModelById(int id)
        {
            var data = _context.Instructors
                .Where(x => x.Id == id)
                .SelectMany(instructor => instructor.Courses)
                .Project().To<CourseModel>();
            return data;
        }
    }
}
