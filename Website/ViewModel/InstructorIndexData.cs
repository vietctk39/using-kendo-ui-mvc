﻿using System.Collections.Generic;
using DataAccess.Entities;
using Website.Models;

namespace Website.ViewModel
{
    public class InstructorIndexData
    {
        public IEnumerable<Instructor> Instructors { get; set; }
        public IEnumerable<Course> Courses { get; set; }
        public IEnumerable<Enrollment> Enrollments { get; set; }
        public InstructorQueryParameters Parameters { get; set; }
    }
}